var tooltips = document.getElementsByClassName("form-tooltip");

for (let i = 0; i < tooltips.length; i++) {
	var descr = tooltips[i].lastChild.lastChild.innerHTML;
	var parentElement = document.createElement("div");
	var childElement = document.createElement("span");
	parentElement.classList.add('tooltip');
	childElement.classList.add('tooltiptext');
	parentElement.appendChild(childElement);
	childElement.innerHTML = descr;
	tooltips[i].insertBefore(parentElement, parent.lastChild);
	tooltips[i].addEventListener('click', function() {
		if (this.classList.contains('active')) {
			this.classList.remove('active');
		} else {
			this.classList.add('active');
		}
	});
}