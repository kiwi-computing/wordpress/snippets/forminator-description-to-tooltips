# Forminator Description to tooltips

Makes it posible to create tooltips from decriptions in wordpress plugin: Forminator

## What to do
* Add css class "form-tooltip" to the fields you want to use tooltips
* Add this php code snippet to functions.php 
```php
add_action('wp_footer', 'tooltip_creater_function');
function tooltip_creater_function() {
	wp_register_script('KC_tooltip_creater', get_theme_file_uri() . '/include/js/tooltip-creater.js','','1', true);
	wp_enqueue_script('KC_tooltip_creater');
}
```
* Add javscript to theme folder 